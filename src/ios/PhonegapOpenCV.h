#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>
#import <Cordova/CDVInvokedUrlCommand.h>

#import "CameraSessionManager.h"
#import "CameraRenderController.h"

@interface PhonegapOpenCV : CDVPlugin
    
- (void) startCamera: (CDVInvokedUrlCommand*)command;
- (void) stopCamera: (CDVInvokedUrlCommand*)command;

- (void) help:(CDVInvokedUrlCommand*)command;

@property (nonatomic) CameraSessionManager *sessionManager;
@property (nonatomic) CameraRenderController *cameraRenderController;
@property (nonatomic) NSString *onPictureTakenHanderId;

@end