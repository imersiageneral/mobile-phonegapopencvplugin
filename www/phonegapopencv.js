var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec');

var PLUGIN_NAME = "PhonegapOpenCV";

var PhonegapOpenCV = function() { };

PhonegapOpenCV.startCamera = function(rect, defaultCamera, tapEnabled, dragEnabled, toBack) {
    exec(null, null, PLUGIN_NAME, "startCamera", [rect.x, rect.y, rect.width, rect.height, defaultCamera, !!tapEnabled, !!dragEnabled, !!toBack]);
};

PhonegapOpenCV.stopCamera = function() {
	exec(null, null, PLUGIN_NAME, "stopCamera", []);
};

PhonegapOpenCV.help = function (name, successCallback, errorCallback) {
    exec(successCallback, errorCallback, PLUGIN_NAME, "help", [name]);
};

module.exports = PhonegapOpenCV;